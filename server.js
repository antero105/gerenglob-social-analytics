'use strict';
/*
	Packages
 */

var https = require('https');
require('http').globalAgent.maxSockets = Infinity;
require('https').globalAgent.maxSockets = Infinity;
var config = require('./config');
var t = require('./twitterObject');
https.globalAgent.maxSockets =  Infinity;
/*
	----------------------------------------------------------------------
 */

/*
	Se crea un hilo por cada cuenta que se tiene en el config.js
 */
config.forEach(function(entry){
	t.twitterAnalisis(entry);
})


