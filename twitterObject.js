'use strict';
var Twit = require('twit');
var mongojs = require('mongojs');
/*
	Objeto que analiza todo lo correspondiente a tweet de cada una de las cuentas. 
	Revisa si el tweet recibido por el Streaming API es un replie, retweet, favorite o mention
	Guarda toda la informacion correspondiente 
 */
var twitterAnalisis = function(user_config){
	var T = new Twit(user_config);
	var stream = T.stream("user", {"with": "user"});
	console.log('Twitter Analisis Starting, Congreso: '+user_config['name']);

	stream.on('tweet', function (tweet) {
		if(isAReply(tweet,user_config['account_id'])){
			updateDB('replie',user_config['account_id'],tweet,user_config['name']);
		}
	
		if(isARetweet(tweet,user_config['account_id'])){
			updateDB('retweet',user_config['account_id'],tweet,user_config['name']);
		}
	
		if(isAMention(tweet,user_config['account_id'])){

			updateDB('mention',user_config['account_id'],tweet,user_config['name']);
		}
	
	});

	stream.on('favorite',function (tweet){
  		if(isAFav(tweet,user_config['account_id'])){
  			updateDB('favorite',user_config['account_id'],tweet,user_config['name']);
  		}
	});

	stream.on('follow',function (tweet){
  		if(isAFollow(tweet,user_config['account_id']))
  			updateDB('follower',user_config['account_id'],tweet,user_config['name']);
	});
	stream.on('disconnect', function (disconnectMessage) {
 		console.log(disconnectMessage);
 	});
 	stream.on('reconnect', function (request, response, connectInterval) {
 		console.log('Somethings Happen');
 		console.log('Reconnecting...');
	});

};
/*
	Condicionales si el trigger del Streaming API recibe algo, se realiza una condicion para saber si fue
	un reply,retweet,favorite o un mention. De igual manera se con estos condicionales
	Se sabe si lo anteriormente mencionado fue hacia la cuenta. Y no la cuenta haciendolo.

 */
function isAReply(tweet,account_id){
	if(tweet.in_reply_to_status_id != null && tweet.in_reply_to_status_id_str != null && tweet.in_reply_to_user_id != null
		&& tweet.in_reply_to_user_id_str != null && tweet.in_reply_to_screen_name != null  && tweet.user.id_str != account_id)
		return true;
	else
		return false;
}
function isARetweet(tweet,account_id){
	if(typeof tweet.retweeted_status != 'undefined' && tweet.user.id_str != account_id)
		return true;
	return false;
}

function isAFollow(tweet,account_id){
	if(tweet.event == 'follow'  && tweet.source.id_str != account_id)
		return true;
	return false;
}
function isAMention(tweet,account_id){
	if(typeof tweet.entities.user_mentions != 'undefined' && tweet.user.id_str != account_id && tweet.entities.user_mentions.length > 0 &&
		tweet.in_reply_to_status_id == null && tweet.in_reply_to_status_id_str == null && tweet.in_reply_to_user_id == null
		&& tweet.in_reply_to_user_id_str == null && tweet.in_reply_to_screen_name == null && typeof tweet.retweeted_status == 'undefined')
			return true;
	return false; 
}
function isAPhoto(tweet,account_id,type){
	if(type == 1)
		if(typeof tweet.entities.media != 'undefined'  && tweet.user.id_str != account_id)
			return true;
	if(type == 2)
		if(typeof tweet.target_object.entities.media != 'undefined' && tweet.source.id_str != account_id)
			return true
	return false;
}
function isAUrl(tweet,account_id,type){
	if(type ==1){

	}

}
function isAFav(tweet,account_id){
	if(tweet.source.id_str != account_id)
		return true;
	return false;
}

/*
	Funcion que actualiza la db depende de lo que se recibio en el Streaming API
 */
function updateDB(type,account_id,tweet,congreso){
	if(typeof type == 'string'){
		if(type == 'replie'){
			var db = mongojs('gerenglobSocialAnalytics',['replies']);
			db.replies.insert({
				tweet:tweet.text,
				account_id : account_id,
				date : new Date(),
				congreso : congreso
			},function(err,saved){
				if(!err || saved)
					console.log('Reply Saved!! in: '+congreso);
				else
					console.log(err);
			});
			
		};
		if(type == 'retweet'){
			var db = mongojs('gerenglobSocialAnalytics',['retweets']);
			var query = {
				$set:{
					tweet:tweet.text,
					date : new Date(),
					type : 'text',
					congreso : congreso
				},
				$inc:{
					followers_count : tweet.user.followers_count
				},
				$push:{
					hashtags:{
							$each:[]
					}
				}
			};
			query.$push.hashtags.$each = tweet.entities.hashtags.map(function(elem) {
				return '#'+elem.text	
			});
			if(isAPhoto(tweet,account_id,1)){
				query.$set.type = 'Photo'
			}
			db.retweets.update({
				account_id : account_id,
				tweet_id : tweet.source.id,
			},query,{upsert:true},function(err,saved){
				if(!err || saved)
					console.log('Retweet saved!! in: '+congreso);
			});
			
			
		};
		if(type == 'mention'){
			var db = mongojs('gerenglobSocialAnalytics',['mentions']);
			db.mentions.update({
				account_id : account_id,
				tweet_id : tweet.id
			},{
				$set:{
					tweet:tweet.text,
					date : new Date(),
					congreso : congreso
					
				},
				$inc:{
					followers_count : tweet.user.followers_count
				},
				$push:{
					hastags:{
							hashtag : tweet.entities.hashtags
					}
				}
			},{upsert:true},function(err,saved){
				if(!err || saved)
					console.log('Mention saved!! in: '+congreso);
				else
					console.log(err);
			});
		};

		if(type == 'follower'){
			var db = mongojs('gerenglobSocialAnalytics',['followers']);
			db.followers.update({
				account_id : account_id,
				congreso:congreso
			},{
				$set:{
					date : new Date(),
					congreso:congreso,
					location : tweet.source.location
				},	
				$inc:{
					followers : tweet.source.followers_count
				}
			},{upsert:true},function(err,saved){
				if(!err||saved)
					console.log("Follower Saved!! in: "+congreso);
				else
					console.log(err);
			});
		}
		if(type == 'favorite'){
			var db = mongojs('gerenglobSocialAnalytics',['favorites']);
			db.favorites.update({
				account_id:account_id,
				tweet_id : tweet.id
			},{
				$set:{
					date : new Date(),
					congreso: congreso,
					location : tweet.source.location,
					tweet : tweet.target_object.text,
					tweet_id : tweet.target_object.id,
					type :'text'
				},
				$inc:{
					followers : tweet.source.followers_count
				}
			},{upsert:true},function(err,saved){
				if(!err || saved)
					console.log("Favorite Saved!! in: "+congreso);
				else
					console.log(err);
			});
			if(isAPhoto(tweet,account_id,2)){
				
				db.favorites.update({
					account_id : account_id,
					tweet_id : tweet.target_object.id
				},{
					$set:{
						type : 'Photo'
					}
				},{},function(err,saved){
					if(!err||saved)
						console.log('Photo Updated!! in: '+congreso);
					else
						console.log(err);
				});
			}
		}

	}
	else{
		console.log("Type can only be \"replie\",\"retweet\",\"mention\",\"follower\" and \"favorite\" ");
	}

	
}
exports.twitterAnalisis  = twitterAnalisis;